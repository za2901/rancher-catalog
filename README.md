
1. Installing daemon

There is script used to install daemon in the system.
Type easy 'install-daemon' and it installs all necessary scripts
in proper path.
edged source file -> share/edge/edged
library files     -> share/edge/lib
packages          -> share/edge/packages
It installs files in /opt/ path. To change it set evironment variable
DEST_DIR to your value

2. Preparing daemon to work

After instaling open file /etc/init.d/edged and verify if all values are set.
If you did it, run command:
update-rc edged defaults
or
systemctl daemon-reload
sudo systemctl enable edged.service
depends on which init system you use
